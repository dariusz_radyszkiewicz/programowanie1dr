package calculator;

public class Main {

    public static void main(String[] args) {

        Example.RED.something();
        Example.GREEN.something();
        Example.BLUE.something();

        //Example checker

        System.out.println(Example.RED.getDesc());
        System.out.println(Example.GREEN.getDesc());
        System.out.println(Example.BLUE.getDesc());

        System.out.println("------------------");

        //Calculator checker

        System.out.println(Calculator.ADD.calculate(4, 4));
        System.out.println(Calculator.SUBTRACT.calculate(5, 3));
        System.out.println(Calculator.MULTIPLY.calculate(6, 5));

    }
}
