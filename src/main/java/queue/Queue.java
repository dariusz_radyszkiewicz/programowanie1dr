package queue;

public interface Queue<T> {

    public void offer(T element);
    T poll();
    T peek();
    int size();

}
