package queue;

public class Link<T>{

    private T value;
    private Link<T> next;

    public T getValue() {
        return value;
    }

    public Link() {

    }

    public void setValue(T value) {
        this.value = value;
    }

    public Link<T> getNext() {
        return next;
    }

    public void setNext(Link<T> next) {
        this.next = next;
    }

    public Link(T value) {
        this.value = value;
    }

}
