package queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T>{

    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    // Kopia tablicy gdzie jeje wielkosc bedzie zawsze size + 1
    // new ArrayQueue
    // []
    // [] <- 145
    // [] -> copyOf(stara tablica, rozmiar starej + 10
    // [ ,] -> elements[elements.lenght - 1] ->145
    // [145]
    // [145] <- 200
    // Arrays.copyOf([145], [145, })
    // elements[elements.lenght - 1] -> [145, 200]
    // Arrays.copyof()
    // elements[elements.lenght -1] = element;

    @Override
    public void offer(T element) {

        elements = Arrays.copyOf(elements, elements.length + 1);

        elements[elements.length - 1] = element;

    }

    @Override
    // podbiera pierwszy element i go zwraca oraz usuwa z kolejki
    public T poll() {
        //pobranie 1 elementu z kolejki

        if (checkIfArrayIsEmpty()) return null;

        T element = (T) elements[0];

        elements = Arrays.copyOfRange
                (elements, 1, elements.length);
        return element;
    }



    //DRY - DONT REPEAT YOURSELF


    @Override
    public T peek() {

        if (checkIfArrayIsEmpty()) return null;

        T element = (T) elements[0];

        return element;
    }

    @Override
    public int size() {
        return elements.length;
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }
}
