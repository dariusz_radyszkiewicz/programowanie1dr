package queue;

public class LinkedQueue<T> implements Queue<T> {

    //Czoło kolejki

    private Link<T> head;

    @Override

    /*
    sprawdzic czy mamy kolo kolejki
     jesli nie to  ustawic na nim nowy link
     jesli mamy czolo kolejki to w petli przebiegamy
     po nastepnych ogniwach az dojedziemy do pustego
     czyli daj nastepnego nic nie zwraca
     [LINK, LINK, LINK, ] <- offer 50
     [LINK -> daj nastepnego, LINK - > daj nastepnego, ...]
     */

    public void offer(T element) {

        Link<T> newLink = new Link<>();
        if(head == null) {
            head = newLink;
        } else {
            // mamy czolo, przebiegamy w petli
            // dopoki badane ogniwo ma kolejny element
            // idziemy do "ogona"
            Link<T> link = this.head;
            while (link.getNext() != null) {
                // iterujemy az dojdziemy do ogona
                link = link.getNext();
            }
            link.setNext(newLink);
        }

    }

    @Override
    public T poll() {

        //jezeli kolejka jest pusta
        if (head == null) {
            System.out.println("Empty queue");
            return null;
        } // jezeli jest to pobieramy wartosc head'a
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }

    @Override
    public int size() {
        // Zadanie domowe
        return 0;
    }
}
