package hospital;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


//        System.out.println("Witaj w rejestracji wybierz: " + "\n" +
//                "a. Rejestracja nowego pacjenta " + "\n" +
//                "b. Obsługa pacjenta " + "\n" +
//                "c. Ilość pacjentów w kolejce " + "\n" +
//                "d. Następny pacjent" + "\n" +
//                "q. Wyjście");



        String option;
        HospitalQueueService hospitalQueueService = new HospitalQueueService();

        do {
            printMenu();
            option = scanner.nextLine();
            if ("a".equals(option)) {
                System.out.println("Rejestracja nowego pacjenta");
                Patient newPatient = handleNewPatient(scanner);
                hospitalQueueService.addPatient(newPatient);
            } else if ("b".equals(option)) {
                Patient handledPatient = hospitalQueueService.handlePatient();
                printPatientInfo(handledPatient);

            } else if ("c".equals(option)) {
                System.out.println(hospitalQueueService.queueSize());
            } else if ("d".equals(option)) {
                System.out.println(hospitalQueueService.nextPatient());
            }
        } while (!"q".equals(option));



    }

    private static void printMenu() {
        System.out.println("Witaj w rejestracji wybierz: " + "\n" +
                "a. Rejestracja nowego pacjenta " + "\n" +
                "b. Obsługa pacjenta " + "\n" +
                "c. Ilość pacjentów w kolejce " + "\n" +
                "d. Następny pacjent" + "\n" +
                "q. Wyjście");
    }

    private static void printPatientInfo(Patient handledPatient) {
        System.out.println(
                new StringBuilder()
                .append("Pacjent ")
                .append(handledPatient.getName())
                .append(" ")
                .append(handledPatient.getSurname())
                .append(" został przyjęty")
                .toString()
        );
    }

    private static Patient handleNewPatient(Scanner scanner) {
        System.out.println("Imię: ");
        String name = scanner.nextLine();
        System.out.println("Nazwisko: ");
        String surname = scanner.nextLine();
        System.out.println("Złość: ");
        int howAngry = scanner.nextInt();
        System.out.println("Choroba: ");
        scanner.nextLine();
        String diseaseStringValue = scanner.nextLine();
        Disease disease = Disease.valueOf(diseaseStringValue);

        Patient patient = new Patient();
        patient.setName(name);
        patient.setSurname(surname);
        patient.setHowAngry(howAngry);
        patient.setDisease(disease);
        return patient;
    }



}
