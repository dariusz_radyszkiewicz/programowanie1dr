package hospital;

public enum Disease {

    FLU(1),
    COLD(2),
    DIARRHEA(3),
    STH_SERIOUS(4);

    private int infectiousness;

    public int getInfectiousness() {
        return infectiousness;
    }

    Disease(int infectiousness) {
        this.infectiousness = infectiousness;
    }
}
