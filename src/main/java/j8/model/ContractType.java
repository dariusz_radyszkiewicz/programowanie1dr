package j8.model;

public enum ContractType {

    F("Full"),
    H("Half");

    private String description;

    ContractType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
