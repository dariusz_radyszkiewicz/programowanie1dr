package j8.fi;

import java.util.Objects;

@FunctionalInterface
public interface MyConsumer<T> {

    void accept(T t);

    default MyConsumer<T> andThen(MyConsumer<T> other) {

        Objects.requireNonNull(other); //sprawdza czy Consumer nie jest nullem

        return (T t) -> {

            this.accept(t);
            other.accept(t);

        };
    }
}
