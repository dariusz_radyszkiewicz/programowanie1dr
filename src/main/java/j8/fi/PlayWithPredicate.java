package j8.fi;

import java.util.function.Predicate;

public class PlayWithPredicate {

    public static void main(String[] args) {

        Predicate<String> checkA = (String t) -> t.contains("A");

        System.out.println(checkA.test("Ania"));
        System.out.println(checkA.test("Erwin"));

        Predicate<String> checkEmpty = (String t) -> t.isEmpty();

        System.out.println(checkEmpty.test("Darek"));
        System.out.println(checkEmpty.test(""));


    }




}
