package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;


public class PlayWithStream {

    public static void main(String[] args) {

        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

//        ex1(employees);
//        ex2(employees);
//        ex3(employees);
//        ex4(employees);
//        ex5(employees);
//        ex6(employees);
//        ex7(employees);
//        ex8(employees);
//        ex9(employees);
//        ex10(employees);
//        ex11(employees);
//        ex12(employees);
//        ex13(employees);
//        ex14(employees);
//        ex14a(employees);
//        ex14b(employees);
        ex15(employees);
//        System.out.println(employees);

        //Profesja do dużej litery

//        employees.stream()
//                .map(e -> e.getFirstName() + " " + Character.toUpperCase(e.getProfession().charAt(0)) + e.getProfession().substring(1))
//                .collect(Collectors.toList())
//                .forEach(System.out::println);
//
//        //Wybieranie z listy osoby której nazwisko kończy się na "ska", jej wiek jest pażysty oraz ma pełen etat
//
//        System.out.println("-------------------------------");
//
//        employees.stream()
//                .filter(g -> g.getAge() % 2 == 0)
//                .filter(g -> g.getLastName().endsWith("ska"))
//                .filter(g -> g.getType() == ContractType.F)
//                .map(g -> g.getFirstName() + " " + g.getLastName() + " " + g.getAge() + " " + g.getType())
//                .forEach(System.out::println);
//
//        System.out.println("Zajęcia - Sobota 1.02.2020");
//
//        List<Employee> empOver50 = employees.stream()
//                .filter(p -> p.getAge() > 50)
//                .collect(Collectors.toList());
//
//        System.out.println(empOver50);
//
//        Map<String, String> collect = employees.stream()
//                .filter(p -> p.getAge() > 50)
//                .collect(
//                        Collectors.toMap(e -> e.getFirstName(), e -> e.getLastName())
//                );
//
//        System.out.println(collect);
//
//        System.out.println(" ");
//
//        Map<String, Set<Employee>> map = employees.stream()
//                .collect(
//                        Collectors.groupingBy(
//                                e -> e.getFirstName(), Collectors.toSet()
//                        )
//                );



    }

    private static void ex15(List<Employee> employees) {

        Optional<Employee> collect = employees.stream()
                .collect(
                        maxBy(
                                Comparator.comparing(Employee::getSalary)
                        )
                );

        System.out.println(collect);

        System.out.println(employees.stream()
                .mapToDouble(e -> e.getSalary())
                .summaryStatistics());

    }

    private static void ex14b(List<Employee> employees) {

        Map<String, Long> a = employees.stream()



                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                filtering(
                                        e -> e.getFirstName().endsWith("a"),
                                        counting()

                                )
                        )
                )
                ;

        System.out.println(a);
    }

    private static void ex14a(List<Employee> employees) {

        Map<String, Integer> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName(),
                                collectingAndThen(
                                        counting(),
                                        value -> value.intValue()
                                )
                        )
                );
        System.out.println(collect);

    }

    private static void ex14(List<Employee> employees) {

        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getFirstName()
                        )
                );

        System.out.println(collect);

    }

    private static void ex1(List<Employee> employees) {

        employees.stream()
                .filter(e -> e.getSalary() > 2600 && e.getSalary() < 3199)
                .forEach(System.out::println);

    }

    private static void ex2(List<Employee> employees) {

        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);

    }

    private static void ex3(List<Employee> employees) {

        employees.stream()
                .filter(g -> g.getLastName().endsWith("ska"))
                .filter(g -> g.getType() == ContractType.F)
                .forEach(System.out::println);

    }

    private static void ex4(List<Employee> employees) {

        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);

    }

    private static void ex5(List<Employee> employees) {

        employees.stream()
                .map(Employee::getLastName)
                .map(e -> e.substring(e.length() - 3))
                .map(e -> (e.endsWith("ski") || e.endsWith("ska")) ? e.toUpperCase() : e) //jeżeli boolean bedzie true wykona sie to co po ? a jeśli false to wykona się to co po ":"
                .forEach(System.out::println);

    }

    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getProfession())
                .map(String::toUpperCase)
                .forEach(e -> System.out.println(e));

    }

    private static void ex7(List<Employee> employees) {

        boolean kowalska = employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalska"));

        System.out.println(kowalska);

    }

    private static void ex8(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        e -> e.getSalary() * 1.12, toList()
                                )
                        )
                );

        System.out.println(collect);
    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getFirstName().charAt(1) == 'a' && e.getLastName().charAt(3) == 'b')
                .forEach(System.out::println);

    }

    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(Comparator.comparing(Employee::getLastName).thenComparing(Employee::getFirstName));

//      To jest długa wersja wcześniejszej linijki:

//                        (person1, person2) -> {
//                            int lastNameComparator = person1.getLastName().compareTo(person2.getLastName());
//
//                            if(lastNameComparator != 0) {
//                                return lastNameComparator;
//                            }
//
//                            return person1.getFirstName()
//                                    .compareTo(person2.getFirstName());
//                        }
//                )
//
//                .forEach(System.out::println);


    }

    private static void ex11(List<Employee> employees) {
        String lastName = employees.stream()
                .map(e -> e.getLastName())
                .collect(Collectors.joining(","));
        System.out.println(lastName);

    }

    private static void ex12(List<Employee> employees) {

        Map<Boolean, Long> collect = employees.stream()
                .collect(
                        partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.counting())
                );

        System.out.println(collect);


    }

    private static void ex13(List<Employee> employees) {

        //wyciąga ze streama pracowników ze wzgledu na rodzaj zatrudnienia

        Map<String, Double> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getType().name(),
                                Collectors.averagingDouble(
                                        e -> e.getSalary()
                                )
                        )
                );

        System.out.println(collect);


    }
}
