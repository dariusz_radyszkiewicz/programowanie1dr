package j8.stream;

import j8.model.ContractType;
import j8.model.Employee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {


    static List load(Path path) {
        try {
            Stream<String> linesOfCsvFile = Files.lines(path);

            return linesOfCsvFile.skip(1) //Stream<String>
                    .map(String::trim)
                    .map(e -> e.split(",")) //Stream<String[]>
                    //. map(e -> mapToObject(e)) wersja bez method reference
                    .map(FileUtils::mapToObject)
                    // .forEach(e -> System.out.println(t)) wersja bez method reference
                    //.forEach(System.out::println)
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }

    public static Employee mapToObject(String[] empArray) {
        String firstName = empArray[0];
        String lastName = empArray[1];
        int age = Integer.parseInt(empArray[2]);
        String profession = empArray[3];
        double salary = Double.parseDouble(empArray[4]);
        ContractType contractType = ContractType.valueOf(empArray[5]);
        return new Employee(firstName, lastName, age, profession, salary, contractType);
    }
}
