package lock;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj kombinacje do zamknięcia zamka w formacie 1-2-4");

        String[] lockCombination = scanner.nextLine().split("-");

        //Cięcie lockCombination aby nie brał pod uwagę "-"

        Lock lock = new Lock(
                Integer.parseInt(lockCombination[0]),
                Integer.parseInt(lockCombination[1]),
                Integer.parseInt(lockCombination[2])
        );

        System.out.println("Tworzę zamek lock ==> " + lock);

        lock.shuffle();
        System.out.println(lock);

        lock.switchA();
        lock.switchC();
        System.out.println(lock);

        if (lock.isOpen()) {
            System.out.println("Zamek jest otwarty");
        } else {
            System.out.println("Zamek jest zamknięty");
        }

    }
}
