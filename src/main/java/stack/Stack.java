package stack;

public class Stack {

    private int array[];
    private int top;
    private int capacity;

    //Stack stack = new Stack(3)
    // stack.push(1)
    // stack.push(50)
    // stack.pop()




    public Stack(int capacity) {
        this.capacity = capacity;
        this.array = new int[capacity];
        this.top = -1;
    }

    public void push(int element) {
        if (isFull()) {
            System.out.println("Stack if full");
        } else {
        array[++top] = element; // najpierw zwieksz o jeden
//        array[top++] = element; // najpierw wez a pozniej zwieksz
        }
    }

    private boolean isFull() {

        return top == capacity - 1;

    }

    private boolean isEmpty() {

        return top == -1;

    }

    public int pop(){
        return array[top--];
    }

    public int peek() {
        return array[top];
    }


}
