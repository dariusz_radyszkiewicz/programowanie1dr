package factorial;

import java.util.Scanner;

public class Factorial {


    int factorial(int n) {
        if (n >= 1) {
            return n * factorial(n - 1);
        } else {
            return 1;
        }
    }

    public static void main(String[] args) {

        Factorial factorial = new Factorial();
        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj liczbę:");
        int finish = factorial.factorial(scanner.nextInt());

        System.out.println("Wynik: " + finish);

    }
}
