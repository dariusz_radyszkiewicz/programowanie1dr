package adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {

    public static void main(String[] args) throws IOException {
        // mass / 3  -> wynik zaokrąglić w dół - 2 --> paliwo
        // dla kazdego modulu liczymy paliwo
        // nastepnie sumujemy wszystko

        int sum = 0;


       Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day1input"));

       List<String> listOfModuleMass = inputValues.collect(Collectors.toList());

        System.out.println(listOfModuleMass);

        for (String mass: listOfModuleMass) {
            int requiredFuel = calculateFuel(Integer.parseInt(mass));
            sum += requiredFuel;
            System.out.println("Fueles needed for mass " + mass + " ->>>> " + requiredFuel);
        }




        System.out.println("Sum of required fuel: " + sum);

    }

    public static int calculateFuel (int mass) {
        int fuel = (mass / 3) - 2;
        if (fuel <= 0) {
            return 0;
        } else {
            return fuel + calculateFuel(fuel);
        }
    }

}
