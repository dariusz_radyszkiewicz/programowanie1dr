package adventofcode;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

import java.util.stream.Stream;

public class Day2 {

    public static void main(String[] args) throws IOException {

        //Tworze pustą tablice Integerów


        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day2input"));

        //Tworze tablice stringów i przycinam

        String[] awsomeData = inputValues.toArray(String[]::new)[0].split(",");

        Integer[] opcode = new Integer[awsomeData.length];

        for (int x = 0; x < awsomeData.length; ++x) {
            opcode[x] = Integer.parseInt(awsomeData[x]);
        }

        opcode[1] = 12;
        opcode[2] = 2;

    }
}
