package palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj słowo:");
        String word = scanner.nextLine();

        boolean isPalidrom = true;
        int start = 0;
        int end = word.length() - 1;

        while (start <= end) {
            System.out.println("Checking:" + word.charAt(start) + " vs " + word.charAt(end));
            if (word.charAt(start) != word.charAt(end)) {
                isPalidrom = false;
                break;
            }
            start++;
            end--;
        }

        if (isPalidrom) {
            System.out.println("Podane słowo: " + word + " kajest Palindromem");
        } else {
            System.out.println("Podane słowo: " + word + " nie jest Palindromem");
        }

    }
}
