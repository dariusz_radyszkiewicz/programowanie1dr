#### Stos (ang. stack)  
###### -> Stos książek, stos talerzy do zmywania

LIFO (ang. Last In, First Out) – czyli ostatni na wejściu, pierwszy na wyjściu.

       |           |
       |   ****    |     API:
       |     **    |     - push (arg)
       |  *******  |     - pop () -> return item + removes
       |  *******  |     - peek () -> ref to top item
       |___________|  
       
       
         array[n]
               ^
               |        
       capacity
       
           top -> pointer to current position
           
         -1 [, , , , ]
          ^
          |
         top
         
            [1, 2 ,4 , , ]
                   ^
                   |
                  top