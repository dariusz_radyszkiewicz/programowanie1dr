package anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj pierwszy wyraz: ");
        String word1 = scanner.nextLine();

        System.out.println("Podaj drugi wyraz: ");
        String word2 = scanner.nextLine();

        char[] firstWordCharacters = word1.toCharArray(); //wrzuca stringa do tablicy znaków
        char[] secondWordCharacters = word2.toCharArray();

        Arrays.sort(firstWordCharacters); //sortuje w kolejności alfabetycznej chary z wczesniej zmienionego stringa
        Arrays.sort(secondWordCharacters);

        boolean areEqual = Arrays.equals(firstWordCharacters, secondWordCharacters);

        if (areEqual) {
            System.out.println("Podane słowa są anagramami");
        } else {
                System.out.println("Podane słowa nie są anagramami");
            }
        }




    }
