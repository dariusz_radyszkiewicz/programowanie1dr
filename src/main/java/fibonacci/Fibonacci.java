package fibonacci;

import java.util.Scanner;

public class Fibonacci {


    int fibonacciCheck(int n) {
        if (n >= 3) {
            return fibonacciCheck(n - 1) + fibonacciCheck(n - 2);
        } else {
            return 1;
        }
    }

    public static void main(String[] args) {

        Fibonacci fibonacci = new Fibonacci();
        Scanner scanner = new Scanner(System.in);

        int checkIt = scanner.nextInt();

        System.out.println(fibonacci.fibonacciCheck(checkIt));
    }
}
